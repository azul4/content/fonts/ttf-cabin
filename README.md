# ttf-cabin

The Cabin font family is a humanist sans with 4 weights and true italics, inspired by Edward Johnston’s and Eric Gill’s typefaces, with a touch of modernism.

https://github.com/impallari/Cabin

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/fonts/ttf-cabin.git
```
